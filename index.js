// console.log("Hello World!");


// JAVASCRIPT has built-in functions and methods for arrays. This allows us to manipulate and access array items




// MUTATOR METHODS



/*

- Mutator methods are functions that "mutate" or change an array after they're created.

- These mutator methods manipulate the original attay perfoming various tasks such as adding and removing elements. 

*/








// -------------------------------------------



// PUSH () - add an element using FUNCTION 




/*

- Adds an element in the end of an array AND returns the array's length

- Syntax
            arrayName.push();
*/





let fruits = ['Apple', 'Orange', 'Chico', 'Lemon'];
console.log('Current array: ');
console.log(fruits);


let fruitslength = fruits.push('Mango');
console.log(fruitslength);
console.log(fruits);








// -------------------------------------------



// WITHOUT RETURN



 // This function does not really updates the original array



function addFruit(fruit) {
	fruits.push(fruit);
	console.log(fruits);
}

addFruit('Gum gum fruit');



// WITH RETURN


function addfruit(newFruit) {
	fruits.push(newFruit);
	console.log(fruits);
	return newFruit;
}

let addedFruit = addFruit("Gum gum fruit");
console.log("addedFruit");





// push () - it will show the total # of the elements or items








// -------------------------------------------




// POP ()


/* 

- Removes the last element in an array AND return the removed element 

- Syntax
	arrayName.pop();

*/



// REMOVING THE LAST ELEMENT IN AN ARRAY


let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method: ');
console.log(fruits);









// -------------------------------------------




// UNSHIFT


/*

- ADDS ONE OR MORE elements at the beginning of an array 

- Syntax
	arrayName.unshift('elementA');
	arrayName.unshift('elementA', 'elementB');

*/



// ADDING an ELEMENT AT THE BEGINNING OF AN ARRAY


fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method: ');
console.log(fruits);







// -------------------------------------------


// SHIFT


/*

- REMOVES an elements at the beginning of an array AND returns the removed element.

- Syntax
	arrayName.shift();
	

*/



// REMOVING an ELEMENT AT THE BEGINNING OF AN ARRAY



let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method: ");
console.log(fruits);







// -------------------------------------------




// SPLICE


/* 

- Simultaneously removes elements from a specified index number and adds element/elements

- Syntax
	arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

*/



fruits.splice(2, 3, 'Coconut', 'Tomato', 'Atis');
console.log('Mutated array from splice method: ');
console.log(fruits);



// ANOther EXAMPLE 


fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('Mutated array from splice method: ');
console.log(fruits);







// -------------------------------------------




// SORT ()


/* 

- RE-arranges the array elements in alphanumeric order

- Syntax
	arrayName.sort();

*/




fruits.sort();
console.log('Mutated array from sort method: ');
console.log(fruits);







// -------------------------------------------




// REVERSE ()


/* 

- Reverses the order of array elements

- Syntax
	arrayName.reverse();

*/



fruits.reverse();
console.log('Mutated array from sort method: ');
console.log(fruits);










// -------------------------------------------
// -------------------------------------------



// NON-MUTATOR METHODS


/* 

- non-mutator methods are functions that do not modify or change an array after they're created

- These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output

*/




let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];


let firstIndex = countries.indexOf('PH');
console.log("Result of indexOf('PH): " + firstIndex);


let invalidCountry = countries.indexOf('BR');
console.log("Result of indexOf('BR'): " + invalidCountry);



let indexOfSecondPH = countries.indexOf('PH', 2);
console.log("Result of indexOf('PH'): " + indexOfSecondPH);



/*

indexof()

- it returns the index # of the matching element
- if no match was found, the result will be -1
- the search process will be done from first element proceeding to the last element

- Syntax:
	arrayName.indexOf(searchValue);
	arrayName.indexOf(searchValue, fromIndex);

*/
 







// -------------------------------------------



// LASTINDEXOF



/*

indexof()

- Returns the index number of the last matching element found in an array;

- The search process will be done from last element proceeding to the first element

- Syntax:
	arrayName.lastNameOf(searchValue);
	arrayName.lastIndexOf(searchValue, fromIndex);

*/



// Getting the index number starting from the last element


let lastIndex = countries.lastIndexOf('PH');
console.log("Result of lastIndexOf() method: " + lastIndex);



// YOU want to search the first PH:


let lastIndexStart = countries.lastIndexOf('PH', 4);
console.log("Result of lastIndex() method: " + lastIndexStart);











// -------------------------------------------



// TOSTRING ()



let stringArray = countries.toString();
console.log("Result from toString() method: ");
console.log(stringArray);
console.log(countries);






// -------------------------------------------



// CONCAT ()


// JinoJOin ang Strings




/*

- Combines two arrays and returns the combined result 

- Syntax:
	arrayA.concat(arrayB);
	arrayA.concat(elementA);

*/




// COMBINING 2 ARRAYS


let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe bootstrap'];
let tasksArrayC = ['get git', 'cook node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log(tasks);






// COMBINING MULTIPLE ARRAYS


let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);







// COMBINING ARRAYS WITH ELEMENTS / ADD ANOTHER ELEMENT


let combinedTasks = tasksArrayA.concat('smell express', 'have react');
console.log(combinedTasks);










// -------------------------------------------



// JOIN ()



/*

- Returns an array as a string separated by specified separator string

- Syntax:
	arrayName.join('separator');
	arrayA.concat(elementA);

*/


let users = ['Join', 'Jane', 'Joe', 'Robert'];
console.log(users.join(' - '));











// -------------------------------------------
// -------------------------------------------



// ITERATION METHODS





// FOREACH ()


/*

 - Syntax:
	arrayName.forEach(function(indivElement) {
	statement
})

*/




// ALLTask

// ['drink html', 'eat javascript', 'inhale css', 'breathe bootstrap', 'get git', 'cook node']



allTasks.forEach(function(task) {
	console.log(task);
});



// ANOTHER Example



let filteredTasks = [];

allTasks.forEach(function(perElement) {
	if (perElement.length > 10) {
		filteredTasks.push(perElement);
	}
});

console.log("Result of filteredTasks: ");
console.log(filteredTasks);






// -------------------------------------------



// MAP ()



/*

- Iterates on each element AND returns new array with different values depending on the result of the function's operation

- This is useful for performing tasks where mutating/changing the elements are required

- Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation

 - Syntax:
	let/const resultArray = arrayName.map(function(indivElement))

*/




let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(element) {
	return element * element;
});

console.log(numbers);
console.log(numberMap);







// -------------------------------------------



// MAP () VS FOREACH ()



// map returns a new array
// forEach doesn't return a new array



let numberForEach = numbers.forEach(function(element){
	return element * 10;
})

console.log(numberForEach);










// -------------------------------------------



// EVERY ()



/*

- Checks if all elements in an array meet the given condition

- This is useful for validating data stored in arrays especially when dealing with large amounts of data

- Returns a true value if all elements meet the condition and false if otherwise

- Syntax
  let/const resultArray = arrayName.every(function(indivElement) {
  return expression/condition;
})

*/




// numbers = [1, 2, 3, 4, 5];



let allValid = numbers.every(function(element) {
    return (element < 3);
});
console.log("Result of every method:");
console.log(allValid);








// -------------------------------------------



// SOME ()



let someValid = numbers.some(function(element) {
	return(element < 3);
});

console.log("Result of some method: ");
console.log(someValid);







// -------------------------------------------



// FILTER ()


// filters out per element if what element is valid



let filteredValid = numbers.filter(function(element) {
	return (element < 3);
});

console.log(filteredValid)








// -------------------------------------------



// NO ELEMENTS FOUND ()



let nothingFound = numbers.filter(function(element) {
	return (element == 0);
});

console.log(nothingFound);










// -------------------------------------------



// INCLUDES () - TRUE or FALSE ang result



/*

- includes() method checks if the argument passed can be found in the array.

- it returns a boolean which can be saved in a variable.

- returns true if the argument is found in the array.

- returns false if it is not.

- Syntax:
            arrayName.includes(<argumentToFind>)

*/




let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");
console.log(productFound1);



// Another EXAMPLE the element that is not included in the list



let productFound2 = products.includes("Headset");
console.log(productFound2);









// -------------------------------------------



// REDUCE



 /* 

- Evaluates elements from left to right and returns/reduces the array into a single value

- Syntax
   let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {return expression/operation})

- The "accumulator" parameter in the function stores the result for every iteration of the loop

- The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop

- How the "reduce" method works
  1. The first/result element in the array is stored in the "accumulator" parameter
  2. The second/next element in the array is stored in the "currentValue" parameter
  3. An operation is performed on the two elements
  4. The loop repeats step 1-3 until all elements have been worked on

*/





// numbers = [1, 2, 3, 4, 5];


let i = 0;
let reduceArray = numbers.reduce(function(acc, cur) {
	console.warn('current iteration' + ++i);
	console.log('accumulator: ' + acc);
	console.log('current value: ' + cur);

	return acc + cur;
});

console.log("Result of reduce method: ");
console.log(reduceArray);















